import data_util
from SimpleMLP import Net
import torch
import numpy as np
import time
from batch import Batch
'''
datashape
18724 784
'''
if __name__ == "__main__":
    print 'meow'
    x, y = data_util.load_data('data')
    y = np.asarray(y[:,0], dtype=np.int64)
    net = Net(784, 10)
    x_tensor = torch.from_numpy(x)
    y_tensor = torch.from_numpy(y)
    t = time.time()
    net.train(x_tensor, y_tensor)
    t2 = time.time()
    print ('time per step ', t2-t)
    t3 = time.time()

    batch = Batch(x, y)

    for e in range(1,20):
        batch.reset_batch(shuffle=True)
        while(batch.has_batch()):
            x_batch, y_batch = batch.get_batch()
            x_tensor = torch.from_numpy(x_batch)
            y_tensor = torch.from_numpy(y_batch)
            loss = net.train(x_tensor, y_tensor)
        print loss
    t4 = time.time()

    # evaluate
    batch.reset_batch(shuffle=True)
    correct = 0
    total = 0
    while (batch.has_batch()):
        x_batch, y_batch = batch.get_batch()
        x_tensor = torch.from_numpy(x_batch)
        y_tensor = torch.from_numpy(y_batch)
        pred = net.predict(x_tensor)
        _, predicted = torch.max(pred.data, 1)
        total += y_tensor.size(0)
        correct += (predicted[:,0] == y_tensor).sum()
    print 'correct ', correct, '/', total
    print 'percentage', (100*float(correct)/total)

    print ('train time ', t4-t3)