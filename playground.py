import torch.nn as nn
import torch
from torch.autograd import Variable
from MeowRNN import MeowRNN
from batch import Batch
import prepare_name_data
from sklearn.model_selection import train_test_split

# m = nn.Linear(20, 30)
# x = Variable(torch.randn(128, 20))
# o = m(x)
# print 'break'

# lstm = nn.LSTM(input_size=300, hidden_size=128, num_layers=32, batch_first=True)

# lstm example
def lstm_example():
    lstm = nn.LSTM(input_size=300, hidden_size=128, num_layers=32, batch_first=True)
    batch_size = 7
    x = Variable(torch.randn(batch_size, 30, 300))
    h0 = Variable(torch.randn(30, batch_size, 128))
    c0 = Variable(torch.randn(30, batch_size, 128))
    o, hn = lstm(x)

    print 'break'

def train_meowrnn():
    n_letter = 58
    n_hidden = 128
    n_classes = 18
    rnn = MeowRNN(n_letter, n_hidden, n_classes)

    x_names, y_names, key_categories = prepare_name_data.load_npy()

    X_train, X_test, y_train, y_test = train_test_split(
        x_names, y_names, test_size=0.33, random_state=42)


    batch = Batch(X_train, y_train)
    test_batch = Batch(X_test, y_test)
    for e in range(1, 20):
        batch.reset_batch(shuffle=True)
        while batch.has_batch():
            input_x, input_y = batch.get_batch()
            # x (64, 10, 1, 58)
            # y (64)
            x_tensor = torch.from_numpy(input_x).float()
            y_tensor = torch.from_numpy(input_y)
            loss = rnn.train(x_tensor, y_tensor)
        print('epoch ', e, 'loss ', loss)

    # evaluate on train data
    total = 0
    correct = 0
    test_batch.reset_batch(shuffle=True)
    while test_batch.has_batch():
        input_x, input_y = test_batch.get_batch()
        # x (64, 10, 1, 58)
        # y (64)
        x_tensor = torch.from_numpy(input_x).float()
        y_tensor = torch.from_numpy(input_y)
        pred = rnn.predict(x_tensor)
        _, predicted = torch.max(pred.data, 1)
        total += y_tensor.size(0)
        correct += (predicted[:, 0] == y_tensor).sum()
    print (correct, '/', total, 'percentage ', 100 * float(correct) / total)


if __name__ == "__main__":
    train_meowrnn()
    print 'break'