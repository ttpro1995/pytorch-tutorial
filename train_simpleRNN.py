from SimpleRNN import SimpleRNN
import prepare_name_data
import numpy as np
import torch
from batch import Batch
from sklearn.model_selection import train_test_split

def train_single():
    n_letter = 57
    n_hidden = 128
    n_classes = 18
    x_names, y_names, key_categories = prepare_name_data.load_npy()
    x_names = x_names[:3000]
    y_names = y_names[:3000]

    rnn = SimpleRNN(n_letter, n_hidden, n_classes)

    # input_x = np.asarray(x_names[0])
    # input_y = np.asarray([y_names[0]])
    # x_tensor = torch.from_numpy(input_x).float()
    # y_tensor = torch.from_numpy(input_y)
    # rnn.train(x_tensor,y_tensor)

    for e in range(1, 10):
        for i in range(len(x_names)):
            input_x = np.asarray(x_names[i])
            input_y = np.asarray([y_names[i]])
            x_tensor = torch.from_numpy(input_x).float()
            y_tensor = torch.from_numpy(input_y)
            loss = rnn.train(x_tensor, y_tensor)
            if (i % 1000 ==0):
                print (i, e)
                print('loss', loss)

    total = 0
    correct = 0
    for i in range(len(x_names)):
        input_x = np.asarray(x_names[i])
        input_y = np.asarray([y_names[i]])
        x_tensor = torch.from_numpy(input_x).float()
        y_tensor = torch.from_numpy(input_y)
        pred = rnn.predict(x_tensor)
        _, predicted = torch.max(pred.data, 1)
        total += y_tensor.size(0)
        correct += (predicted[:, 0] == y_tensor).sum()
    print 'correct ', correct, '/', total
    print 'percentage', (100 * float(correct) / total)

def train_batch():
    n_letter = 58
    n_hidden = 128
    n_classes = 18
    x_names, y_names, key_categories = prepare_name_data.load_npy()

    X_train, X_test, y_train, y_test = train_test_split(
        x_names, y_names, test_size=0.33, random_state=42)

    rnn = SimpleRNN(n_letter, n_hidden, n_classes)

    batch = Batch(X_train, y_train)
    test_batch = Batch(X_test, y_test)
    for e in range(1, 20):
        batch.reset_batch(shuffle=True)
        while  batch.has_batch():
            input_x, input_y = batch.get_batch()
            # x (64, 10, 1, 58)
            # y (64)
            x_tensor = torch.from_numpy(input_x).float()
            y_tensor = torch.from_numpy(input_y)
            loss = rnn.train(x_tensor, y_tensor)
        print('epoch ', e,'loss ', loss)

    # evaluate on train data
    total = 0
    correct = 0
    test_batch.reset_batch(shuffle=True)
    while test_batch.has_batch():
        input_x, input_y = test_batch.get_batch()
        # x (64, 10, 1, 58)
        # y (64)
        x_tensor = torch.from_numpy(input_x).float()
        y_tensor = torch.from_numpy(input_y)
        pred = rnn.predict(x_tensor)
        _, predicted = torch.max(pred.data, 1)
        total += y_tensor.size(0)
        correct += (predicted[:, 0] == y_tensor).sum()
    print (correct, '/', total, 'percentage ', 100*float(correct)/total)

if __name__ == "__main__":
    
    train_batch()