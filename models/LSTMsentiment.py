import torch.nn as nn
import torch
from torch.autograd import Variable
import torch.nn.functional as F
import numpy as np
import torch.optim as optim


class LSTMsentiment(nn.Module):
    def __init__(self, indim, hiddim_lstm, hiddim_mlp, n_classes, is_cuda = False):
        super(LSTMsentiment,self).__init__()
        self.is_cuda = is_cuda
        if (is_cuda):
            self.cuda()
        self.indim = indim
        self.hiddim_lstm = hiddim_lstm
        self.hiddim_mlp = hiddim_mlp

        self.lstm = nn.LSTM(input_size=indim, hidden_size=hiddim_lstm, num_layers=1, batch_first=True)

        # two-layer MLP
        self.l1 = nn.Linear(hiddim_lstm, hiddim_mlp)
        self.l2 = nn.Linear(hiddim_mlp, hiddim_mlp)
        self.l3 = nn.Linear(hiddim_mlp, n_classes)

        self._optimizer = optim.SGD(self.parameters(), lr=0.001, weight_decay=0.00003)
        self._criterion = nn.CrossEntropyLoss()

        if is_cuda:
            self.lstm = self.lstm.cuda()
            self.l1 = self.l1.cuda()
            self.l2 = self.l2.cuda()
            self.l3 = self.l3.cuda()
            self._criterion = self._criterion.cuda()


    def forward(self, input, training = False):
        # input: (batch_size, seq, indim)
        # input: (batch_size, indim)
        # hidden: (batch_size, hdim)
        o, hn = self.lstm(F.dropout(input, p=0.5, training=training))
        o = o[:, -1]  # get output of last layer

        h1 = F.tanh(self.l1(F.dropout(o, p=0.2, training=training)))
        h2 = F.tanh(self.l2(F.dropout(h1, p=0.2, training=training)))
        pred = F.softmax(self.l3(F.dropout(h2, p=0.2, training=training)))

        return pred

    def train(self, x, y):
        self._optimizer.zero_grad()

        if self.is_cuda:
            x = x.cuda()
            y = y.cuda()
        x_tensor = Variable(x)
        y_tensor = Variable(y)


        output = self.forward(x_tensor[:,:,0], training=True)

        loss = self._criterion(output, y_tensor)
        loss.backward()
        self._optimizer.step()
        return loss

    def predict(self, x):
        if self.is_cuda:
            x = x.cuda()
        x_tensor = Variable(x)
        output = self.forward(x_tensor[:, :, 0])
        return output
