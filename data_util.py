import numpy as np
import os
from scipy import ndimage
import matplotlib.pyplot as plt
import argparse

image_size = 28
pixel_depth = 255


def load_letter(folder, min_num_images):
    """Load the data for a single letter label."""
    image_files = os.listdir(folder)
    dataset = np.ndarray(shape=(len(image_files), image_size, image_size),
                         dtype=np.float32)
    print(folder)
    num_images = 0
    for image in image_files:
        image_file = os.path.join(folder, image)
        try:
            image_data = (ndimage.imread(image_file).astype(float) -
                          pixel_depth / 2) / pixel_depth
            if image_data.shape != (image_size, image_size):
                raise Exception('Unexpected image shape: %s' % str(image_data.shape))
            dataset[num_images, :, :] = image_data
            num_images = num_images + 1
        except IOError as e:
            print('Could not read:', image_file, ':', e, '- it\'s ok, skipping.')

    dataset = dataset[0:num_images, :, :]
    if num_images < min_num_images:
        raise Exception('Many fewer images than expected: %d < %d' %
                        (num_images, min_num_images))

    print('Full dataset tensor:', dataset.shape)
    print('Mean:', np.mean(dataset))
    print('Standard deviation:', np.std(dataset))
    return dataset

def load_and_pickle(notMNIST_dir, output_dir, min_num_image):
    """
    Load all data in notMNIST_dir and save as npy
    :param notMNIST_dir:
    :param min_num_image:
    :return:
    """
    sub_dir = os.listdir(notMNIST_dir)

    for d in sub_dir:
        set_filename = d
        d = notMNIST_dir+'/'+d
        dataset = load_letter(d, min_num_image)
        np.save(output_dir+'/'+set_filename,dataset)



def merge_datasets(data_dir):
    letters = os.listdir(data_dir)
    datasets = None
    labels = None

    for index ,letter in enumerate(letters):
        data_tensor = np.load(data_dir+'/'+letter)
        l = np.ones(shape=(data_tensor.shape[0]))
        l = l*index
        if (datasets == None):
            datasets = data_tensor
            labels = l
        else:
            datasets = np.vstack((datasets, data_tensor))
            labels = np.hstack((labels, l))
    return datasets, labels




def save_full_dt():
    dt, l = merge_datasets('./pdata')
    np.save('full_data',dt)
    np.save('full_label', l)

def load_full_dt():
    dt = np.load('full_data.npy')
    l = np.load('full_label.npy')
    return dt, l
# save_full_dt()


def randomize(dataset, labels):
    """
    shuffled dataset
    :param dataset:  x
    :param labels:   y
    :return:
    """
    permutation = np.random.permutation(labels.shape[0])
    shuffled_dataset = dataset[permutation,:,:]
    shuffled_labels = labels[permutation]
    return shuffled_dataset, shuffled_labels

# dt, l = load_full_dt()
# sd, sl = randomize(dt,l)



def preprocess_dataset(raw_letter_data_dir):
    """
    load image and process to tensor
    :param raw_letter_data_dir: notMNIST dataset dir
    :return:
    data tensors and labels as x and y
    """
    sub_dir = os.listdir(raw_letter_data_dir)
    datasets = None
    labels = None
    for index ,letter in enumerate(sub_dir):
        # data
        data_tensor = load_letter(raw_letter_data_dir+'/'+letter, 0)
        # label
        l = np.ones(shape=(data_tensor.shape[0]))
        l = l*index
        if (datasets == None):
            datasets = data_tensor
            labels = l
        else:
            datasets = np.vstack((datasets, data_tensor))
            labels = np.hstack((labels, l))
    # randomize data
    datasets, labels = randomize(datasets, labels)
    return datasets, labels

def store_data(datasets, labels, dir):
    """
    save data to npy
    :param datasets: full x tensor
    :param labels: label y
    :param dir: directory to save data
    :return:
    """
    if not os.path.exists(dir):
        os.makedirs(dir)
    np.save(dir+'/x', datasets) # always treat that user forgot slash
    np.save(dir+'/y', labels)



# dt, l = preprocess_dataset('data/notMNIST_small')
# store_data(dt, l, 'save/data/')

def load_data(dir):
    x = np.load(dir+'/x.npy')
    y = np.load(dir+'/y.npy')
    x = x.reshape(x.shape[0], -1)
    y = y.reshape(y.shape[0], -1)
    return x, y

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--raw', '-r', default=0,
                        help='Raw notMNIST dataset dir')
    parser.add_argument('--out', '-o', default=0,
                        help='Preprocessed notMNIST pickle (as numpy) dir')
    args = parser.parse_args()

    raw_letter_data_dir = args.raw
    preprocessed_dir = args.out
    print (raw_letter_data_dir)
    print (preprocessed_dir)

    x, y = preprocess_dataset(raw_letter_data_dir)
    store_data(x, y, preprocessed_dir)


# print('break')