import torch
from torch.autograd import Variable
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim

class Net(nn.Module):
    def __init__(self, indim, n_classes):
        super(Net,self).__init__()
        self.l1 = nn.Linear(indim,200)
        self.l2 = nn.Linear(200, 200)
        self.l3 = nn.Linear(200, n_classes)

        self._optimizer = optim.SGD(self.parameters(), lr=0.01)
        self._criterion = nn.CrossEntropyLoss()

    def _forward(self, train, x_batch, y_batch = None):
        x = Variable(x_batch)
        h1 = self.l1(x)
        h2 = self.l2(h1)
        pred= F.softmax(self.l3(h2))
        loss = None
        if train:
            y = Variable(y_batch)
            loss = self._criterion(pred, y)
        return pred, loss

    def train(self, x_batch, y_batch):
        self._optimizer.zero_grad()
        pred, loss = self._forward(True, x_batch, y_batch)
        loss.backward()
        self._optimizer.step()
        return loss

    def predict(self, x_batch):
        return self._forward(False, x_batch)[0]

if __name__ == "__main__":
    pass