import torch
from torch.autograd import Variable
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
import torch.optim as optim

class Net(nn.Module):
    def __init__(self):
        super(Net,self).__init__()
        indim = 3
        n_classes = 5
        self.l1 = nn.Linear(indim,200)
        self.l2 = nn.Linear(200, 200)
        self.l3 = nn.Linear(200, n_classes)

    def forward(self, train, x_batch, y_batch = None):
        x = Variable(x_batch)
        # x = x_batch
        h1 = self.l1(x)
        h2 = self.l2(h1)
        y = F.softmax(self.l3(h2))
        accum_loss = 0 if train else None
        if train:
            criterion = nn.CrossEntropyLoss()
            accum_loss = criterion(y, target)

        return y, accum_loss

if __name__ == "__main__":
    net = Net()
    print(net)

    input = Variable(torch.randn(3,3))
    x = np.random.rand(3,3)
    x = np.array(x, dtype=np.float32)
    x = torch.from_numpy(x)
    print ('input ', input)
    print ('x ', x)
    target = Variable(torch.LongTensor([0,1,2]))
    out = net.forward(True, x, target)
    print(out)

    optimizer = optim.SGD(net.parameters(), lr = 0.01)



    print 'break'
