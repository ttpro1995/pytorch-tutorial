import numpy as np
from SimpleRNN import SimpleRNN
from batch import Batch
import torch
import time
import models.LSTMsentiment

import meowlogtool.log_util as log_util
from meowlogtool.log_util import StreamToLogger
import sys
if __name__ == "__main__":
    # attach meow log tool
    logger = log_util.create_logger("NTI_sentiment", True)
    logstream = StreamToLogger(logger)
    sys.stdout = logstream
    print '-------------Start------------------'

    batch = Batch(dir='saved_train')
    dev_batch = Batch(dir='saved_dev')
    test_batch = Batch(dir='saved_test')
    x, y = batch.get_batch()
    # x shape (64, 32, 1, 300)
    # y shape (64)
    is_cuda = True

    # model = SimpleRNN(300, 128, 2, is_cuda=is_cuda)
    # TODO: run this experiement with LSTM sentiment
    model = models.LSTMsentiment(300, 300, 200, 2, is_cuda)
    start_t = time.time()
    print ('Model ', type(model).__name__)

    for e in range(1, 3):
        batch.reset_batch(shuffle=True)
        while batch.has_batch():
            input_x, input_y = batch.get_batch()
            # x (64, 10, 1, 58)
            # y (64)
            x_tensor = torch.from_numpy(input_x).float()
            y_tensor = torch.from_numpy(input_y)
            loss = model.train(x_tensor, y_tensor)
        print('epoch ', e, 'loss ', loss)

        # evaluate on dev data
        total = 0
        correct = 0
        dev_batch.reset_batch(shuffle=True)
        while dev_batch.has_batch():
            input_x, input_y = dev_batch.get_batch()
            # x (64, 10, 1, 58)
            # y (64)
            x_tensor = torch.from_numpy(input_x).float()
            y_tensor = torch.from_numpy(input_y)
            if is_cuda:
                y_tensor = y_tensor.cuda()
            pred = model.predict(x_tensor)
            _, predicted = torch.max(pred.data, 1)
            total += y_tensor.size(0)
            correct += (predicted[:, 0] == y_tensor).sum()
        print ('dev ',correct, '/', total, 'percentage ', 100 * float(correct) / total)



        # evaluate on train data
        total = 0
        correct = 0
        test_batch.reset_batch(shuffle=True)
        while test_batch.has_batch():
            input_x, input_y = test_batch.get_batch()
            # x (64, 10, 1, 58)
            # y (64)
            x_tensor = torch.from_numpy(input_x).float()
            y_tensor = torch.from_numpy(input_y)
            if is_cuda:
                y_tensor = y_tensor.cuda()
            pred = model.predict(x_tensor)
            _, predicted = torch.max(pred.data, 1)
            total += y_tensor.size(0)
            correct += (predicted[:, 0] == y_tensor).sum()
        print ('test', correct, '/', total, 'percentage ', 100 * float(correct) / total)

    end_t = time.time()

    print ('total time', end_t - start_t)
    print '-------------DONE------------------'
