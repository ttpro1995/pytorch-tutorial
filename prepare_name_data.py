# -*- coding: utf-8 -*-
import glob
import torch
import numpy as np
import codecs

def findFiles(path): return glob.glob(path)



import unicodedata
import string

all_letters = string.ascii_letters + " .,;'"+"_"
n_letters = len(all_letters)

# Turn a Unicode string to plain ASCII, thanks to http://stackoverflow.com/a/518232/2809427
def unicodeToAscii(s):
    return ''.join(
        c for c in unicodedata.normalize('NFD', s)
        if unicodedata.category(c) != 'Mn'
        and c in all_letters
    )
#
# print(unicodeToAscii(u'Ślusàrski'))
# Build the category_lines dictionary, a list of names per language
# category_lines = {}
# all_categories = []

# Read a file and split into lines
def readLines(filename):
    lines = codecs.open(filename,'r',encoding="utf-8").read().strip().split('\n')
    return [unicodeToAscii(line) for line in lines]

#
# for filename in findFiles('data/names/*.txt'):
#     category = filename.split('/')[-1].split('.')[0]
#     all_categories.append(category)
#     lines = readLines(filename)
#     category_lines[category] = lines
#
# n_categories = len(all_categories)



# Find letter index from all_letters, e.g. "a" = 0
def letterToIndex(letter):
    return all_letters.find(letter)

# Just for demonstration, turn a letter into a <1 x n_letters> Tensor
def letterToVec(letter):
    vec = np.zeros(shape=(1, n_letters))
    vec[0, letterToIndex(letter)] = 1
    # vec = torch.zeros(1, n_letters)
    # vec[0][letterToIndex(letter)] = 1
    return np.array(vec)

# Turn a line into a <line_length x 1 x n_letters>,
# or an array of one-hot letter vectors

def padding_word(word, length = 10):
    word = word[:length]
    while (len(word) < length):
        word = '_'+word
    return word


def lineToVec(line, length = 10):
    line = padding_word(line)
    line_vec = letterToVec(line[0])
    for i in range(1, len(line)):
        letter_vec = letterToVec(line[i])
        line_vec = np.vstack((line_vec, letter_vec))
    line_vec = line_vec.reshape(line_vec.shape[0], 1, line_vec.shape[1])
    return line_vec

def prepare_npy():
    category_lines = {}
    all_categories = []
    key_categories = []
    for filename in findFiles('data/names/*.txt'):
        category = filename.split('/')[-1].split('.')[0]
        all_categories.append(category)
        lines = readLines(filename)
        key_categories.append(category)
        category_lines[category] = lines
    x_names = []
    y_names = []
    for idx, val in enumerate(key_categories):
        lines = category_lines[val]
        for line in lines:
            x = lineToVec(line)
            x_names.append(x)
            y_names.append(idx)

    x_names = np.array(x_names)
    y_names = np.array(y_names)
    key_categories = np.array(key_categories)

    np.save('saved/x_names',x_names)
    np.save('saved/y_names',y_names)
    np.save('saved/key_categories',key_categories)

def load_npy():
    x_names = np.load('saved/x_names.npy')
    y_names = np.load('saved/y_names.npy')
    key_categories = np.load('saved/key_categories.npy')
    per = np.random.permutation(len(x_names))
    x_names = x_names[per]
    y_names = y_names[per]
    return x_names, y_names, key_categories

if __name__ == "__main__":
    # x_names, y_names, key_categories = load_npy()
    prepare_npy()

    print 'breakpoint'