import torch.nn as nn
import torch
from torch.autograd import Variable
import torch.nn.functional as F
import numpy as np
import torch.optim as optim
import data_util

import prepare_name_data

class MeowRNN(nn.Module):
    def __init__(self, indim, hiddim, n_classes, is_cuda = False):
        super(MeowRNN,self).__init__()
        self.is_cuda = is_cuda
        if (is_cuda):
            self.cuda()
        self.indim = indim
        self.hiddim = hiddim

        self.lstm = nn.LSTM(input_size=indim, hidden_size=128, num_layers=2, batch_first=True, dropout=0.2)
        self.decoder = nn.Linear(hiddim, n_classes)

        if is_cuda:
            self.lstm = self.lstm.cuda()

        self._optimizer = optim.SGD(self.parameters(), lr=0.01)
        self._criterion = nn.CrossEntropyLoss()

    def forward(self, input):
        # input: (batch_size, seq, indim)
        # input: (batch_size, indim)
        # hidden: (batch_size, hdim)
        o, hn = self.lstm(input)
        o = o[:, -1]  # get output of last layer
        pred = self.decoder(o)
        return pred

    def train(self, x, y):
        self._optimizer.zero_grad()

        if self.is_cuda:
            x = x.cuda()
            y = y.cuda()
        x_tensor = Variable(x)
        y_tensor = Variable(y)


        output = self.forward(x_tensor[:,:,0])

        loss = self._criterion(output, y_tensor)
        loss.backward()
        self._optimizer.step()
        return loss

    def predict(self, x):
        if self.is_cuda:
            x = x.cuda()
        x_tensor = Variable(x)
        output = self.forward(x_tensor[:, :, 0])
        return output


if __name__ == "__main__":
    n_letter = 57
    n_hidden = 128
    n_classes = 18
    rnn = MeowRNN(n_letter, n_hidden, n_classes)


    print 'breakpoint'