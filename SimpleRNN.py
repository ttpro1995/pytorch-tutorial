import torch.nn as nn
import torch
from torch.autograd import Variable
import torch.nn.functional as F
import numpy as np
import torch.optim as optim

import prepare_name_data

class SimpleRNN(nn.Module):
    def __init__(self, indim, hiddim, n_classes, is_cuda = False):
        super(SimpleRNN,self).__init__()
        self.is_cuda = is_cuda
        if (is_cuda):
            self.cuda()
        self.indim = indim
        self.hiddim = hiddim

        self.i2h = nn.Linear(indim+hiddim, hiddim)
        self.i2o = nn.Linear(indim+hiddim, n_classes)
        if is_cuda:
            self.i2h = self.i2h.cuda()
            self.i2o = self.i2o.cuda()

        self._optimizer = optim.SGD(self.parameters(), lr=0.01)
        self._criterion = nn.CrossEntropyLoss()

    def forward(self, input, hidden):
        # input: (batch_size, indim)
        # hidden: (batch_size, hdim)
        combined = torch.cat((input, hidden), 1)
        if self.is_cuda:
            combined = combined.cuda()
        hidden = self.i2h(combined)
        output = F.softmax(self.i2o(combined))
        return output, hidden

    def train(self, x, y):
        self._optimizer.zero_grad()

        if self.is_cuda:
            x = x.cuda()
            y = y.cuda()
        x_tensor = Variable(x)
        y_tensor = Variable(y)

        hidden = self.initHidden(batch_size=x_tensor.size()[0])
        for i in range(x_tensor.size()[1]): # for each vector in sample sequence
            output, hidden = self.forward(x_tensor[:,i,0], hidden)
        loss = self._criterion(output, y_tensor)
        loss.backward()
        self._optimizer.step()
        return loss

    def predict(self, x):
        if self.is_cuda:
            x = x.cuda()
        x_tensor = Variable(x)
        hidden = self.initHidden(batch_size=x_tensor.size()[0])
        for i in range(x_tensor.size()[1]):
            output, hidden = self.forward(x_tensor[:,i,0], hidden)
        return output

    def initHidden(self, batch_size = 1):
        zeros = torch.zeros(batch_size, self.hiddim).float()
        if self.is_cuda:
            zeros = zeros.cuda()
        return Variable(zeros)

if __name__ == "__main__":
    n_letter = 57
    n_hidden = 128
    n_classes = 18
    rnn = SimpleRNN(n_letter, n_hidden, n_classes)
    input_x = prepare_name_data.letterToVec('A')
    input_x_t = torch.from_numpy(input_x)
    x = Variable(input_x_t.float())
    hidden = Variable(torch.zeros(1, n_hidden).float())
    print(x.size())
    print(hidden.size())
    output, next_h = rnn.forward(x, hidden)
