import numpy as np
class Batch:
    def __init__(self, x=None, y=None, dub_data=False, dir=None):
        if dir is not None:
            self.x = np.load(dir+'/batch_x.npy')
            self.y = np.load(dir+'/batch_y.npy')
        else:
            self.x = np.array(x)
            self.y = np.array(y)
        self.n_sample = len(self.y)
        assert len(self.y) == len(self.x)
        self.left_idx = 0
        self.dub_data = dub_data

    def get_batch(self, batch_size=64):
        """
        get a batch of batch_size
        :param batch_size: default 32
        :return: x, y as list
        """
        right_idx = batch_size + self.left_idx
        x = self.x[self.left_idx:right_idx]
        y = self.y[self.left_idx:right_idx]
        if (self.dub_data == True):  # dublicate data for tree matching
            x = np.concatenate([x, x])
        y = np.asarray(y, dtype='int64')
        self.left_idx = right_idx  # update left idx for next batch
        return (x, y)

    def has_batch(self):
        """
        Check if we have any batch left
        :return:
        """
        if (self.left_idx < self.n_sample):
            return True
        return False

    def reset_batch(self, shuffle=False):
        self.left_idx = 0

        # permutation dataset
        if (shuffle == True):
            shuffle_len = self.x.shape[0]
            print ('Shuffle len ', shuffle_len)
            per = np.random.permutation(shuffle_len)
            self.x = self.x[per]
            self.y = self.y[per]